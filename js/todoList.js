const todoForm = document.querySelector(".todoForm"),
  todoInput = todoForm.querySelector(".todoInput"),
  todoList = document.querySelector(".todoList");

const TODOS_LS = "toDos";
let toDos = [];

function paintToDo(text) {
  console.log(text);
  const li = document.createElement('li');
  li.innerText = text;
  li.classList.add("todos");
  const newId = toDos.length + 1;
  todoList.appendChild(li);
  li.id = newId;
  const todoObj = {
    text: text,
    id: newId
  };
  li.addEventListener("click",deleteToDo);
  toDos.push(todoObj);
  saveToDos();
}

function saveToDos() {
  localStorage.setItem(TODOS_LS, JSON.stringify(toDos));
}

function handleSubmit(event) {
  event.preventDefault();
  const currentValue = todoInput.value;
  if (todoInput.value !== "" && toDos.length < 13) {
    paintToDo(currentValue);
  }
  todoInput.value = "";

}

function loadElements() {
  const loadedtoDos = localStorage.getItem(TODOS_LS);
  if (loadedtoDos !== null) {
    const parsedToDos = JSON.parse(loadedtoDos);
    parsedToDos.forEach(function(todo) {
      paintToDo(todo.text);
    })
  }
}

function deleteToDo(event){
  const li = event.target;
  todoList.removeChild(event.target);
  const cleanToDos = toDos.filter(function(toDo){
    return toDo.id !== parseInt(li.id);
  });
  toDos = cleanToDos;
  saveToDos();
}

function init() {
  loadElements();
  todoForm.addEventListener("submit", handleSubmit);
}
init();