const dischargeDay = document.querySelector(".dischargeDay");

function displaySeconds(){
  const currentDate = new Date();
  const dischargeDate = new Date(2019,11,15); // yyyy , m-1 , d [2019,12,15]
  const currentMilliSeconds = dischargeDate.getTime() - currentDate.getTime();
  const finishDay = Math.ceil((dischargeDate.getTime() - currentDate.getTime()) / 1000 / 60 / 60 / 24);
  dischargeDay.innerText = `전역까지 이만큼이나 남음 : ${finishDay}일 === ${currentMilliSeconds}MS`;
}

function init(){
  setInterval(displaySeconds,1);
}
init();