const welcomeName = document.querySelector(".welcomeName"),
  nameForm = document.querySelector(".questionNameForm"),
  nameInput = nameForm.querySelector(".questionNameInput"),
  form = document.querySelector(".todoForm"),
  nameDisplay = document.querySelector(".nameDisplay");

const USER_LS = "currentUser",
  SHOWING_ON = "showing",
  FLEX_SHOWING = "flexShowing";

function paintName(text) {
  nameForm.classList.remove(SHOWING_ON);
  nameDisplay.classList.add(FLEX_SHOWING);
  form.classList.remove("form");
  welcomeName.innerText = `Hello ${text}`;
}

function askForName() {
  nameForm.classList.add(SHOWING_ON);
  nameForm.addEventListener("submit", handleSubmit);
}

function handleSubmit(event) {
  event.preventDefault();
  const currentName = nameInput.value;
  nameInput.value = "";

  saveName(currentName);
  paintName(currentName);
}


function saveName(text) {
  localStorage.setItem(USER_LS, text);
}

function loadName() {
  const currentUser = localStorage.getItem(USER_LS);
  if (currentUser === null) {
    askForName();
  } else {
    paintName(currentUser);
  }
}

function init() {
  loadName();
}
init();


// 기본적으로 askName 폼과 paint에 display:none 값을 준 다음
// loadName() 함수로 체크해서 localStorage가 비었으면 askForName() 함수 호출
// localStorage의 값이 있으면 바로 paintName()함수 호출