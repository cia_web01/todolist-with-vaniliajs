const settingBtn = document.querySelector(".setting");
const popUpBox = document.querySelector(".popUpContainer");
const popUpTextName = document.querySelector("#popUpTextName");

const HIDE = "hide";

let hideCheck = true;

function popUpEvent(event) {
  if (hideCheck === true) {
    popUpBox.classList.remove(HIDE);
    hideCheck = false;
  } else {
    popUpBox.classList.add(HIDE);
    hideCheck = true;
  }
}

function resetName() {
//   will reset userName from localStorage.
  localStorage.removeItem("currentUser");
  location.reload(true);
}

function init() {
  settingBtn.addEventListener("click", popUpEvent);
  popUpTextName.addEventListener("click", resetName);
}
init();